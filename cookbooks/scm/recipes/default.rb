#
# Cookbook:: scm
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.

template "/etc/motd" do
source "motd.erb"
end

template "/etc/motd" do
	source "miaplicacion.conf.erb"
	variables({
		:ip=>node['scm']['ip'],
		:usuario=>node['scm']['usuario'],
		:puerto=>node['scm']['puerto']
	})
end
